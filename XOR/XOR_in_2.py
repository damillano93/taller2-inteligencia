"""
Titulo: ADALINE XOR 
Author: David Millan
Date: 27/03/2019
"""

import numpy as np
import matplotlib.pyplot as plt
import math
import csv
LEARNING_RATE = 0.9

# Step function
def step(x):
    if (x > 0):
        return 1
    else:
        return -1;
    



# Compuerta XOR 2 entradas
# la entrada es representada por las dos primeras columnas y la tercera columna corresponde BIAS
INPUTS = np.array([[-1,-1,1],
                   [-1, 1,1],
                   [ 1,-1,1],
                   [ 1, 1,1] ])
# salidas del sistema usando -1 y 1           
OUTPUTS = np.array([[-1,1,1,-1]]).T

# inicializar el random con una semilla 
np.random.seed(1)

# incicializar los pesos de manera aleatoria
WEIGHTS = 2*np.random.random((3,1)) - 1
#print "Pesos iniciales", WEIGHTS

# inicializar la variable error
errors=[]

# ciclo de entrenamiento
for iter in xrange(10000):

    for input_item,desired in zip(INPUTS, OUTPUTS):
       
        # calcula la salida haciendo la sumatoria de las multiplicaciones de los pesos por las entradas
        ADALINE_OUTPUT = (input_item[0]*WEIGHTS[0]) + (input_item[1]*WEIGHTS[1]) + (input_item[2]*WEIGHTS[2])

        # realiza la approximacion de la funcion 
        ADALINE_OUTPUT = step(ADALINE_OUTPUT)

        # calcula el error
        ERROR = desired - ADALINE_OUTPUT
        
        # guarda los errores
        errors.append(ERROR)
        
        # actualiza los pesos basados en la regla delta 
        WEIGHTS[0] = WEIGHTS[0] + LEARNING_RATE * ERROR * input_item[0]
        WEIGHTS[1] = WEIGHTS[1] + LEARNING_RATE * ERROR * input_item[1]
        WEIGHTS[2] = WEIGHTS[2] + LEARNING_RATE * ERROR * input_item[2]
    
    


#print "Nuevos pesos despues del entrenamiento", WEIGHTS
#for input_item,desired in zip(INPUTS, OUTPUTS):
#    # caclula la salida de ADALINE de acuerdo a los pesos 
#    ADALINE_OUTPUT = (input_item[0]*WEIGHTS[0]) + (input_item[1]*WEIGHTS[1]) + (input_item[2]*WEIGHTS[2])
#
#
#
#    print "Respuesta: ", ADALINE_OUTPUT, "Esperado: ", desired
#

# grafica el resultado de los errores
ax = plt.subplot(111)
ax.plot(errors, c='#aaaaff', label='Error de entreamiento')
ax.set_xscale("log")
plt.title("Error ADALINE XOR 2 entradas \n 10000 repeticiones alpha= 0.9" )
plt.legend()
plt.xlabel('Error')
plt.ylabel('Valor')
plt.savefig('xor_in_2_alpha_09.png')